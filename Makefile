all : typecheck pdf

.ONESHELL:
typecheck :
				cd src/
				agda --allow-unsolved-metas **.lagda.md

pdf :
				pandoc -i metadata.yaml src/*.md -o 'PLFA.pdf' \
				--pdf-engine=lualatex -V links-as-notes=true

epub :
				pandoc -i metadata.yaml src/*.md -o 'PLFA.epub' \
				-t epub3 -V links-as-notes=true
