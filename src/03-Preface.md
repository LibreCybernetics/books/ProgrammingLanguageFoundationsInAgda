# Preface {-}

The most profound connection between logic and computation is a pun. The
doctrine of \textsf{Propositions as Types} asserts that a certain kind of formal
structure may be read in two ways: either as a proposition in logic or as a type
in computing. Further, a related structure may be read as either the proof of
the proposition or as a programme of the corresponding type. Further still,
simplification of proofs corresponds to evaluation of programs.

Accordingly, the title of this book also has two readings. It may be parsed as
\textsf{(Programming Language) Foundations in Agda} or \textsf{Programming
(Language Foundations) in Agda} --- the specifications we will write in the
proof assistant \textsf{Agda} both describe programming languages and are
themselves programmes.

The book is aimed at students in the last year of an undergraduate honours
programme or the first year of a master or doctorate degree. It aims to teach
the fundamentals of operational semantics of programming languages, with
\textsf{Simply Typed Lambda Calculus} ($\lambda^{\rightarrow}$) as the central
example. The textbook is written as a literate script in \textsf{Agda}. The hope
is that using a proof assistant will make the development more concrete and
accessible to students, and give them rapid feedback to find and correct
misapprehensions.

The book is broken into two parts. The first part, \textsf{Logical Foundations},
develops the needed formalisms. The second part, \textsf{Programming Language
Foundations}, introduces basic methods of operational semantics.

## Personal remarks

Since 2013, I have taught a course on \textsf{Types and Semantics for
Programming Languages} to fourth-year undergraduates and masters students at the
University of Edinburgh. An earlier version of that course was based on Benjamin
Pierce's excellent [\textsf{TAPL}][tapl]. My version was based of Pierce's
subsequent textbook, [\textsf{Software Foundations}][sf], written in
collaboration with others and based on \textsf{Coq}. I am convinced of Pierce's
claim that basing a course around a proof assistant aids learning, as summarised
in his \textsf{ICFP} Keynote, [\textsf{Lambda, The Ultimate TA}][ta].

However, after five years of experience, I have come to the conclusion that
\textsf{Coq} is not the best vehicle. Too much of the course needs to focus on
learning _tactics_ for proof derivation, to the cost of learning the
fundamentals of programming language theory. Every concept has to be learned
twice: e.g., both the _product data type_, and the corresponding _tactics_ for
_introduction_ and _elimination_ of _conjunctions_. The rules \textsf{Coq}
applies to generate _induction hypotheses_ can sometimes seem mysterious. While
the `notation`{.coq} construct permits pleasingly flexible syntax, it can be
confusing that the same concept must always be given two names, e.g., both
`subst N x M`{.coq} and `N [x := M]`{.coq}. Names of _tactics_ are sometimes
short and sometimes long; naming conventions in the _standard library_ can be
wildly inconsistent. \textsf{Propositions as types} as a foundation of _proof_
is present but hidden.

I found myself keen to recast the course in \textsf{Agda}. In \textsf{Agda},
there is no longer any need to learn about _tactics_: there is just _dependently
typed programming_, plain and simple. _Introduction_ is always by a
_constructor_, _elimination_ is always by _pattern matching_. _Induction_ is no
longer a mysterious separate concept, but corresponds to the familiar notion of
_recursion_. _Mixfix syntax_ is flexible while using just one name for each
concept, e.g., _substitution_ is `_[_:=_]`{.agda}. The _standard library_ is not
perfect, but there is a fair attempt at consistency. \textsf{Propositions as
types} as a foundation of _proof_ is on proud display.

Alas, there is no textbook for programming language theory in \textsf{Agda}.
Stump's [\textsf{Verified Functional Programming in Agda}][stump] covers related
ground, but focuses more on programming with dependent types than on the theory
of programming languages.

The original goal was to simply adapt \textsf{Software Foundations}, maintaining
the same text but transposing the code from \textsf{Coq} to \textsf{Agda}. But
it quickly became clear to me that after five years in the classroom I had my
own ideas about how to present the material. They say you should never write a
book unless you cannot _not_ write the book, and I soon found that this was a
book I could not not write.

I am fortunate that my student, [Wen Kokke][wen], was keen to help. She guided
me as a newbie to \textsf{Agda} and provided an infrastructure that is easy to
use and produces pages that are a pleasure to view.

Most of the text was written during a sabbatical in the first half of 2018.

--- Philip Wadler, Rio de Janeiro, January–June 2018

[tapl]: https://www.cis.upenn.edu/~bcpierce/tapl/
[sf]: https://softwarefoundations.cis.upenn.edu/
[ta]: https://www.cis.upenn.edu/~bcpierce/papers/plcurriculum.pdf
[stump]: https://www.morganclaypoolpublishers.com/catalog_Orig/samples/9781970001259_sample.pdf
[wen]: https://github.com/wenkokke
[phil]: https://homepages.inf.ed.ac.uk/wadler/

## A word on the exercises

 - Exercises labelled \textsf{(recommended)} are the ones students are required to
do in the class taught at Edinburgh from this textbook.
 - Exercises labelled \textsf{(stretch)} are there to provide an extra challenge.
Few students do all of these, but most attempt at least a few.
 - Exercises without a label are included for those who want extra practice.
