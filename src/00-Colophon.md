\pagenumbering{gobble}

\clearpage

`\begin{adjustwidth}{2.5em}{2.5em}`{=latex}

\vspace*{\fill}

\centering

\footnotesize

This is a modified version mainly in regards to formatting. It seeks to be a
verbatim copy of the original.

\vspace{2em}

Typesetting was done with Pandoc and \LuaLaTeX.

\vspace{1em}

The types used are Montserrat, \textsf{Montserrat Alternates} and \texttt{Fira
Code}. (With additional glyphs from \texttt{Fira Math})

\vspace{2em}

This book is under a Peer Production License

< <https://civicwise.org/peer-production-licence-_-human-readable-summary/> >

\vspace{2em}

![](img/lpp.png){width=10em}\

\vspace{2em}

LibreCybernetics

\vspace{1em}

\today

`\end{adjustwidth}`{=latex}

\clearpage
