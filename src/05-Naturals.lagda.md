\ignore{
```agda
module 05-Naturals where
```
}

# Natural numbers

> The night sky holds more stars than I can count, though fewer than five
> thousand are visible to the naked eye. The observable universe contains about
> seventy sextillion stars.

> But the number of stars is finite, while natural numbers are infinite. Count
> all the stars, and you will still have as many natural numbers left over as
> you started with.

## The naturals are an inductive datatype

Everyone is familiar with the natural numbers $0, 1, 2, 3,$ and so on. We write
$\mathbb{N}$ for the _type_ of natural numbers, and say that $0, 1, 2, 3,$ and
so on are _values_ of _type_ $\mathbb{N}$, indicated by writing $0 : \mathbb{N},
1 : \mathbb{N}, 2 : \mathbb{N}, 3 : \mathbb{N},$ and so on.

The _set_ of natural numbers is infinite, yet we can write down its definition
in just a few lines. Here is the definition as a pair of _inference rules_:

\begin{equation}
  \begin{aligned}[t]
    \begin{array}{c}
      \\
      \hline
      zero : \mathbb{N}
    \end{array}
  \end{aligned}
  \hspace{2em}
  \begin{aligned}[t]
    \begin{array}{c}
      m : \mathbb{N} \\
      \hline
      suc\ m : \mathbb{N}
    \end{array}
  \end{aligned}
\end{equation}

\needspace{4\baselineskip}
And here is the definition in \textsf{Agda}:

```agda
data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
```

Here `ℕ`{.agda} is the name of the _datatype_ we are defining, and `zero`{.agda}
and `suc`{.agda} (short for _successor_) are the _constructors_ of the
_datatype_.

\needspace{4\baselineskip}
Both definitions above tell us the same two things:

 - _Base case_: `zero`{.agda} is a natural number.
 - _Inductive case_: if `m`{.agda} is a natural number, then `suc m`{.agda} is
   also a natural number.

Further, these two _rules_ give the _only_ ways of creating natural numbers.
\needspace{6\baselineskip} Hence, the possible natural numbers are:

~~~{.agda}
zero
suc zero
suc (suc zero)
suc (suc (suc zero))
...
~~~

We write `0`{.agda} as shorthand for `zero`{.agda}; and `1`{.agda} is shorthand
for `suc zero`{.agda}, the successor of `zero`{.agda}, that is, the natural that
comes after `zero`{.agda}; and `2`{.agda} is shorthand for `suc (suc
zero)`{.agda}, which is the same as `suc 1`{.agda}, the successor of `1`{.agda};
and `3` is shorthand for the successor of `2`{.agda}; and so on.

#### Exercise `seven` {#seven}

Write out `7`{.agda} in longhand.

## Unpacking the _inference rules_

Let's unpack the _inference rules_. Each _inference rule_ consists of zero or
more _judgments_ written above a horizontal line, called the _hypotheses_, and a
single _judgment_ written below, called the _conclusion_. The first _rule_ is
the _base case_. It has no _hypotheses_, and the _conclusion_ asserts that
$zero$ is a natural. The second _rule_ is the _inductive case_. It has one
_hypothesis_, which assumes that $m$ is a natural, and the _conclusion_ asserts
that $suc\ m$ is a also a natural.

## Unpacking the \textsf{Agda} definition

Let's unpack the \textsf{Agda} definition. The _keyword_ `data`{.agda} tells us
this is an _inductive definition_, that is, that we are defining a new
_datatype_ with _constructors_. \needspace{2\baselineskip} The phrase `ℕ :
Set`{.agda} tells us that `ℕ`{.agda} is the name of the new _datatype_, and that
it is a `Set`{.agda}, which is the way in \textsf{Agda} of saying that it is a
_type_. The _keyword_ `where`{.agda} separates the declaration of the _datatype_
from the declaration of its _constructors_. Each _constructor_ is declared on a
separate line, which is indented to indicate that it belongs to the
corresponding `data`{.agda} declaration. \needspace{3\baselineskip} The lines

~~~{.agda}
  zero : ℕ
  suc  : ℕ → ℕ
~~~

give _signatures_ specifying the _types_ of the _constructors_ `zero`{.agda} and
`suc`{.agda}. They tell us that `zero`{.agda} is a natural number and that
`suc`{.agda} takes a natural number as argument and returns a natural number.

You may have noticed that `ℕ`{.agda} and `→`{.agda} don't appear on your
keyboard. They are symbols in \textsf{unicode}. At the end of each chapter is a
list of all \textsf{unicode} symbols introduced in the chapter, including
instructions on how to type them in the \textsf{Emacs} text editor. Here _type_
refers to typing with fingers as opposed to _datatypes_!

## The story of creation

\needspace{3\baselineskip}
Let's look again at the rules that define the natural numbers:

 - _Base case_: `zero`{.agda} is a natural number.
 - _Inductive case_: if `m`{.agda} is a natural number, then `suc m`{.agda} is
   also a natural number.

Hold on! The second line defines natural numbers in terms of natural numbers.
How can that possibly be allowed? Isn't this as useless a definition as _"Brexit
means Brexit"_?

In fact, it is possible to assign our definition a meaning without resorting to
unpermitted circularities. Furthermore, we can do so while only working with
_finite sets_ and never referring to the _infinite set_ of natural numbers.

We will think of it as a creation story. To start with, we know about no natural
numbers at all:

> In the beginning, there are no natural numbers.

Now, we apply the rules to all the natural numbers we know about. The base case
tells us that `zero`{.agda} is a natural number, so we add it to the _set_ of
known natural numbers. The inductive case tells us that if `m`{.agda} is a
natural number (on the day before today) then `suc m`{.agda} is also a natural
number (today). We didn't know about any natural numbers before today, so the
inductive case doesn't apply:

\needspace{2\baselineskip}
> On the first day, there is one natural number.
>
> - `zero : ℕ`{.agda}

Then we repeat the process. On the next day we know about all the numbers from
the day before, plus any numbers added by the rules. The base case tells us that
`zero`{.agda} is a natural number, but we already knew that. But now the
inductive case tells us that since `zero`{.agda} was a natural number yesterday,
then `suc zero`{.agda} is a natural number today:

\needspace{3\baselineskip}
> On the second day, there are two natural numbers.
>
> - `zero : ℕ`{.agda}
> - `suc zero : ℕ`{.agda}

And we repeat the process again. Now the inductive case tells us that since
`zero`{.agda} and `suc zero`{.agda} are both natural numbers, then `suc
zero`{.agda} and `suc (suc zero)`{.agda} are natural numbers. We already knew
about the first of these, but the second is new:

\needspace{4\baselineskip}
> On the third day, there are three natural numbers.
>
> - `zero : ℕ`{.agda}
> - `suc zero : ℕ`{.agda}
> - `suc (suc zero) : ℕ`{.agda}

You've got the hang of it by now:

> On the fourth day, there are four natural numbers.
>
> - `zero : ℕ`{.agda}
> - `suc zero : ℕ`{.agda}
> - `suc (suc zero) : ℕ`{.agda}
> - `suc (suc (suc zero)) : ℕ`{.agda}

The process continues. On the $n^{\textrm{th}}$ day there will be $n$ distinct
natural numbers. Every natural number will appear on some given day. In
particular, the number $n$ first appears on day $n+1$. And we never actually
define the _set_ of numbers in terms of itself. Instead, we define the _set_ of
numbers on day $n+1$ in terms of the _set_ of numbers on day $n$.

A process like this one is called _inductive_. We start with nothing, and build
up a potentially _infinite set_ by applying rules that convert one _finite set_
into another _finite set_.

The rule defining `zero`{.agda} is called a base case, because it introduces a
natural number even when we know no other natural numbers. The rule defining
`suc`{.agda} is called an inductive case, because it introduces more natural
numbers once we already know some. Note the crucial role of the base case. If we
only had inductive rules, then we would have no numbers in the beginning, and
still no numbers on the second day, and on the third, and so on. An inductive
definition lacking a base case is useless, as in the phrase _"Brexit means
Brexit"_.

## Philosophy and history

A philosopher might observe that our reference to the first day, second day, and
so on, implicitly involves an understanding of natural numbers. In this sense,
our definition might indeed be regarded as in some sense circular, but we need
not let this disturb us. Everyone possesses a good informal understanding of the
natural numbers, which we may take as a foundation for their formal description.

While the natural numbers have been understood for as long as people can count,
the inductive definition of the natural numbers is relatively recent. It can be
traced back to Richard Dedekind's paper \textsf{"Was sind und was sollen die
Zahlen?"} (_What are and what should be the numbers?_), published in 1888, and
Giuseppe Peano's book \textsf{"Arithmetices principia, nova methodo exposita"}
(_The principles of arithmetic presented by a new method_), published the
following year.

## A pragma

In \textsf{Agda}, any text following `--`{.agda} or enclosed between `{- and
-}`{.agda} is considered a _comment_.  _Comments_ have no effect on the code,
with the exception of one special kind of _comment_, called a _pragma_, which is
enclosed between `{-# and #-}`{.agda}.

\needspace{2\baselineskip}
Including the line
```agda
{-# BUILTIN NATURAL ℕ #-}
```

tells \textsf{Agda} that `ℕ`{.agda} corresponds to the natural numbers, and
hence one is permitted to type `0`{.agda} as shorthand for `zero`{.agda},
`1`{.agda} as shorthand for `suc zero`{.agda}, `2`{.agda} as shorthand for `suc
(suc zero)`{.agda}, and so on. The declaration is not permitted unless the
type given has exactly two _constructors_, one with no arguments (corresponding
to $zero$) and one with a single argument the same as the type being defined
(corresponding to $successor$).

As well as enabling the above shorthand, the _pragma_ also enables a more
efficient internal representation of naturals using the \textsf{Haskell} type
for arbitrary-precision integers. Representing the natural $n$ with
`zero`{.agda} and `suc`{.agda} requires space proportional to $n$, whereas
representing it as an arbitrary-precision integer in \textsf{Haskell} only
requires space proportional to $\log{n}$.

## Imports

Shortly we will want to write some equations that hold between terms involving
natural numbers. \needspace{5\baselineskip} To support doing so, we import the
definition of equality and notations for reasoning about it from the
\textsf{Agda} _standard library_:

```agda
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _∎)
```

The first line brings the _standard library module_ that defines equality into
_scope_ and gives it the name `Eq`{.agda}. The second line opens that _module_,
that is, adds all the names specified in the `using`{.agda} _clause_ into the
_current scope_. In this case the names added are `_≡_`{.agda}, the equality
_operator_, and `refl`{.agda}, the name for evidence that two terms are equal.
The third line takes a _module_ that specifies _operators_ to support reasoning
about equivalence, and adds all the names specified in the `using`{.agda}
_clause_ into the _current scope_. In this case, the names added are
`begin_`{.agda}, `_≡⟨⟩_`{.agda}, and `_∎`{.agda}. We will see how these are used
below. We take these as givens for now, but will see how they are defined in the
chapter [Equality][plfa.Equality].

\textsf{Agda} uses underbars to indicate where terms appear in _infix_ or
_mixfix operators_. Thus, `_≡_`{.agda} and `_≡⟨⟩_`{.agda} are _infix_ (each
_operator_ is written between two terms), while `begin_`{.agda} is _prefix_
(it is written before a term), and `_∎`{.agda} is _postfix_ (it is written
after a term).

Parentheses and semicolons are among the few characters that cannot appear in
names, so we do not need extra spaces in the `using`{.agda} list.

## Operations on naturals are recursive functions {#plus}

Now that we have the natural numbers, what can we do with them? For instance,
can we define arithmetic operations such as addition and multiplication?

As a child I spent much time memorising tables of addition and multiplication.
At first the rules seemed tricky and I would often make mistakes. It came as a
shock to me to discover _recursion_, a simple technique by which every one of
the infinite possible instances of addition and multiplication can be specified
in just a couple of lines.

\needspace{4\baselineskip}
Here is the definition of addition in Agda:
```agda
_+_ : ℕ → ℕ → ℕ
zero  + n  =  n
suc m + n  =  suc (m + n)
```

Let's unpack this definition. Addition is an _infix operator_. It is written
with underbars where the argument go, hence its name is `_+_`{.agda}.  The first
line is a _signature_ specifying the type of the operator. The type `ℕ → ℕ →
ℕ`{.agda}, indicates that addition accepts two naturals and returns a natural.
_Infix_ notation is just a shorthand for application; the terms `m + n`{.agda}
and `_+_ m n`{.agda} are equivalent.

The definition has a base case and an inductive case, corresponding to those for
the natural numbers. The base case says that adding `zero`{.agda} to a number,
`zero + n`{.agda}, returns that number, `n`{.agda}. The inductive case says that
adding the successor of a number to another number, `(suc m) + n`{.agda},
returns the successor of adding the two numbers, `suc (m + n)`{.agda}. We say we
use _pattern matching_ when _constructors_ appear on the left-hand side of an
equation.

\needspace{4\baselineskip}
If we write `zero`{.agda} as `0`{.agda} and `suc m`{.agda} as `1 + m`{.agda},
the definition turns into two familiar equations:

~~~{.agda}
0       + n  ≡  n
(1 + m) + n  ≡  1 + (m + n)
~~~

The first follows because `zero` is an identity for addition, and the second
because addition is associative. \needspace{2\baselineskip} In its most general
form, associativity is written

$$(m + n) + p \equiv m + (n + p)$$

meaning that the location of parentheses is irrelevant. We get the second
equation from the third by taking $m$ to be `1`{.agda}, $n$ to be `m`{.agda},
and $p$ to be `n`{.agda}. We write `=`{.agda} for definitions, while we write
`≡`{.agda} for assertions that two already defined things are the same.

The definition is _recursive_, in that the last line defines addition in terms
of addition. As with the inductive definition of the naturals, the apparent
circularity is not a problem. It works because addition of larger numbers is
defined in terms of addition of smaller numbers. Such a definition is called
_well founded_.

\needspace{5\baselineskip}
For example, let's add two and three:
```agda
_ : 2 + 3 ≡ 5
_ =
  begin
    2 + 3
  ≡⟨⟩    -- is shorthand for
    (suc (suc zero)) + (suc (suc (suc zero)))
  ≡⟨⟩    -- inductive case
    suc ((suc zero) + (suc (suc (suc zero))))
  ≡⟨⟩    -- inductive case
    suc (suc (zero + (suc (suc (suc zero)))))
  ≡⟨⟩    -- base case
    suc (suc (suc (suc (suc zero))))
  ≡⟨⟩    -- is longhand for
    5
  ∎
```

\needspace{6\baselineskip}
We can write the same derivation more compactly by only expanding shorthand as
needed:
```agda
_ : 2 + 3 ≡ 5
_ =
  begin
    2 + 3
  ≡⟨⟩
    suc (1 + 3)
  ≡⟨⟩
    suc (suc (0 + 3))
  ≡⟨⟩
    suc (suc 3)
  ≡⟨⟩
    5
  ∎
```

The first line matches the inductive case by taking `m = 1`{.agda} and `n =
3`{.agda}, the second line matches the inductive case by taking `m = 0`{.agda}
and `n = 3`{.agda}, and the third line matches the base case by taking `n =
3`{.agda}.

Both derivations consist of a _signature_ (written with a colon, `:`{.agda}),
giving a _type_, and a _binding_ (written with an equal sign, `=`{.agda}),
giving a term of the given _type_. Here we use the _dummy name_ `_`{.agda}.
The _dummy name_ can be reused, and is convenient for examples. Names other than
`_`{.agda} must be used only once in a _module_.

Here the _type_ is `2 + 3 ≡ 5`{.agda} and the term provides _evidence_ for the
corresponding equation, here written in tabular form as a chain of equations.
The chain starts with `begin`{.agda} and finishes with `∎`{.agda} (pronounced
"qed" or "tombstone", the latter from its appearance), and consists of a series
of terms separated by `≡⟨⟩`{.agda}.

\needspace{4\baselineskip}
In fact, both proofs are longer than need be, and \textsf{Agda} is satisfied
with the following:
```agda
_ : 2 + 3 ≡ 5
_ = refl
```

\textsf{Agda} knows how to compute the value of `2 + 3`{.agda}, and so can
immediately check it is the same as `5`{.agda}. A binary relation is said to be
reflexive if every value relates to itself. Evidence that a value is equal to
itself is written `refl`{.agda}.

In the chains of equations, all \textsf{Agda} checks is that each term
simplifies to the same _value_. If we jumble the equations, omit lines, or add
extraneous lines it will still be accepted. It's up to us to write the equations
in an order that makes sense to the reader.

Here `2 + 3 ≡ 5`{.agda} is a _type_, and the chains of equations (and also
`refl`{.agda}) are terms of the given _type_; alternatively, one can think of
each term as _evidence_ for the assertion $2 + 3 \equiv 5$. This duality of
interpretation --- of a _type_ as a proposition, and of a term as evidence ---
is central to how we formalise concepts in \textsf{Agda}, and will be a running
theme throughout this book.

Note that when we use the word _evidence_ it is nothing equivocal. It is not
like testimony in a court which must be weighed to determine whether the witness
is trustworthy. Rather, it is ironclad. The other word for evidence, which we
will use interchangeably, is _proof_.

#### Exercise `+-example` {#plus-example}

Compute `3 + 4`, writing out your reasoning as a chain of equations.

## Multiplication

\needspace{5\baselineskip}
Once we have defined addition, we can define multiplication as repeated
addition:
```agda
_*_ : ℕ → ℕ → ℕ
zero  * n  =  zero
suc m * n  =  n + (m * n)
```

Computing `m * n`{.agda} returns the sum of `m`{.agda} copies of `n`{.agda}.

\needspace{3\baselineskip}
Again, rewriting turns the definition into two familiar equations:

~~~{.agda}
0       * n  ≡  0
(1 + m) * n  ≡  n + (m * n)
~~~

The first follows because $zero$ times anything is $zero$, and the second
follows because multiplication distributes over addition.
\needspace{3\baselineskip} In its most general form, distribution of
multiplication over addition is written

$$(m + n) \cdot p  \equiv  (m \cdot p) + (n \cdot p)$$

We get the second equation from the third by taking $m$ to be `1`{.agda}, $n$ to
be `m`, and $p$ to be `n`, and then using the fact that `1`{.agda} is an
identity for multiplication, so `1 * n ≡ n`.

Again, the definition is well-founded in that multiplication of larger numbers
is defined in terms of multiplication of smaller numbers.

\needspace{4\baselineskip}
For example, let's multiply two and three:
```agda
_ =
  begin
    2 * 3
  ≡⟨⟩    -- inductive case
    3 + (1 * 3)
  ≡⟨⟩    -- inductive case
    3 + (3 + (0 * 3))
  ≡⟨⟩    -- base case
    3 + (3 + 0)
  ≡⟨⟩    -- simplify
    6
  ∎
```

The first line matches the inductive case by taking `m = 1`{.agda} and `n =
3`{.agda}, The second line matches the inductive case by taking `m = 0`{.agda}
and `n = 3`{.agda}, and the third line matches the base case by taking `n =
3`{.agda}. Here we have omitted the _signature_ declaring `_ : 2 * 3 ≡ 6`, since
it can easily be inferred from the corresponding term.

#### Exercise `*-example` {#times-example}

Compute `3 * 4`, writing out your reasoning as a chain of equations.


#### Exercise `_^_` \textsf{(recommended)} {#power}

\needspace{3\baselineskip}
Define exponentiation, which is given by the following equations:

~~~{.agda}
n ^ 0       ≡  1
n ^ (1 + m) ≡  n * (n ^ m)
~~~

Check that `3 ^ 4` is `81`.

## Monus

We can also define subtraction. Since there are no negative natural numbers, if
we subtract a larger number from a smaller number we will take the result to be
zero. This adaption of subtraction to naturals is called _monus_ (a twist on
_minus_).

\needspace{6\baselineskip}
Monus is our first use of a definition that uses _pattern matching_ against both
arguments:
```agda
_∸_ : ℕ → ℕ → ℕ
m     ∸ zero   =  m
zero  ∸ suc n  =  zero
suc m ∸ suc n  =  m ∸ n
```

\needspace{5\baselineskip}
We can do a simple analysis to show that all the cases are covered.

 - Consider the second argument.
   + If it is `zero`, then the first equation applies.
   + If it is `suc n`, then consider the first argument.
     * If it is `zero`, then the second equation applies.
     * If it is `suc m`, then the third equation applies.

Again, the recursive definition is well-founded because monus on bigger numbers
is defined in terms of monus on smaller numbers.

\needspace{4\baselineskip}
For example, let's subtract two from three:

~~~{.agda}
_ =
  begin
     3 ∸ 2
  ≡⟨⟩
     2 ∸ 1
  ≡⟨⟩
     1 ∸ 0
  ≡⟨⟩
     1
  ∎
~~~

\needspace{5\baselineskip}
We did not use the second equation at all, but it will be required if we try to
subtract a larger number from a smaller one:

~~~{.agda}
_ =
  begin
     2 ∸ 3
  ≡⟨⟩
     1 ∸ 2
  ≡⟨⟩
     0 ∸ 1
  ≡⟨⟩
     0
  ∎
~~~

#### Exercise `∸-examples` \textsf{(recommended)} {#monus-examples}

Compute `5 ∸ 3` and `3 ∸ 5`, writing out your reasoning as a chain of equations.

## Precedence

We often use _precedence_ to avoid writing too many parentheses. Application
_binds more tightly than_ (or _has precedence over_) any _operator_, and so we
may write `suc m + n`{.agda} to mean `(suc m) + n`{.agda}. As another example,
we say that multiplication _binds more tightly than_ addition, and so write `n +
m * n`{.agda} to mean `n + (m * n)`{.agda}. We also sometimes say that addition
_associates to the left_, and so write `m + n + p`{.agda} to mean `(m + n) +
p`{.agda}.

\needspace{4\baselineskip}
In \textsf{Agda} the _precedence_ and _associativity_ of _infix operators_ needs
to be declared:
```agda
infixl 6  _+_  _∸_
infixl 7  _*_
```

This states operators `_+_`{.agda} and `_∸_`{.agda} have precedence level 6, and
operator `_*_`{.agda} has precedence level 7. Addition and monus _bind less
tightly than_ multiplication because they have lower precedence. Writing
`infixl`{.agda} indicatesthat all three operators associate to the left. One can
also write `infixr`{.agda} to indicate that an operator associates to the right,
or just `infix`{.agda} to indicate that parentheses are always required to
disambiguate.

## Currying

We have chosen to represent a function of two arguments in terms of a function
of the first argument that returns a function of the second argument. This trick
goes by the name _currying_.

\textsf{Agda}, like other functional languages such as \textsf{Haskell} and
\textsf{ML}, is designed to make currying easy to use. Function arrows associate
to the right and application associates to the left; `ℕ → ℕ → ℕ`{.agda} stands
for `ℕ → (ℕ → ℕ)`{.agda} and `_+_ 2 3`{.agda} stands for `(_+_ 2) 3`{.agda}

The term `_+_ 2`{.agda} by itself stands for the function that adds two to its
argument, hence applying it to three yields five.

Currying is named for Haskell Curry, after whom the programming language
\textsf{Haskell} is also named. Curry's work dates to the 1930's. When I first
learned about currying, I was told it was misattributed, since the same idea was
previously proposed by Moses Schönfinkel in the 1920's. I was told a joke:

> It should be called schönfinkeling, but currying is tastier.

Only later did I learn that the explanation of the misattribution was itself a
misattribution. The idea actually appears in the \textsf{Begriffschrift} of
Gottlob Frege, published in 1879.

## The story of creation, revisited

Just as our inductive definition defines the naturals in terms of the naturals,
so does our recursive definition define addition in terms of addition.

Again, it is possible to assign our definition a meaning without resorting to
unpermitted circularities. We do so by reducing our definition to equivalent
inference rules for judgments about equality:

\begin{equation}
  \begin{aligned}[t]
    \begin{array}{c}
      n : \mathbb{N} \\
      \hline
      zero + n = n
    \end{array}
  \end{aligned}
  \hspace{2em}
  \begin{aligned}[t]
    \begin{array}{c}
      m + n = p \\
      \hline
      (suc\ m) + n  = suc\ p
    \end{array}
  \end{aligned}
\end{equation}

Here we assume we have already defined the infinite set of natural numbers,
specifying the meaning of the judgment $n : \mathbb{N}$. The first inference
rule is the base case. It asserts that if $n$ is a natural number then adding
zero to it gives $n$. The second inference rule is the inductive case. It
asserts that if adding $m$ and $n$ gives $p$, then adding $suc\ m$ and $n$ gives
$suc\ p$.

Again we resort to a creation story, where this time we are concerned with
judgments about addition:

> In the beginning, we know nothing about addition.

Now, we apply the rules to all the judgment we know about. The base case tells
us that `zero + n = n` for every natural `n`, so we add all those equations. The
inductive case tells us that if `m + n = p` (on the day before today) then `suc
\ m + n = suc\ p` (today). We didn't know any equations about addition before
today, so that rule doesn't give us any new equations:

> On the first day, we know about addition of 0.
>
> - `0 + 0 = 0    0 + 1 = 1    0 + 2 = 2    ⋯`{.agda}

Then we repeat the process, so on the next day we know about all the equations
from the day before, plus any equations added by the rules. The base case tells
us nothing new, but now the inductive case adds more equations:

> On the second day, we know about addition of 0 and 1.
>
> - `0 + 0 = 0    0 + 1 = 1    0 + 2 = 2    0 + 3 = 3    ⋯`{.agda}
> - `1 + 0 = 1    1 + 1 = 2    1 + 2 = 3    1 + 3 = 4    ⋯`{.agda}

And we repeat the process again:

> On the third day, we know about addition of 0, 1, and 2.
>
> - `0 + 0 = 0    0 + 1 = 1    0 + 2 = 2    0 + 3 = 3    ⋯`{.agda}
> - `1 + 0 = 1    1 + 1 = 2    1 + 2 = 3    1 + 3 = 4    ⋯`{.agda}
> - `2 + 0 = 2    2 + 1 = 3    2 + 2 = 4    2 + 3 = 5    ⋯`{.agda}

You've got the hang of it by now:

> On the fourth day, we know about addition of 0, 1, 2, and 3.
>
> - `0 + 0 = 0    0 + 1 = 1    0 + 2 = 2    0 + 3 = 3    ⋯`{.agda}
> - `1 + 0 = 1    1 + 1 = 2    1 + 2 = 3    1 + 3 = 4    ⋯`{.agda}
> - `2 + 0 = 2    2 + 1 = 3    2 + 2 = 4    2 + 3 = 5    ⋯`{.agda}
> - `3 + 0 = 3    3 + 1 = 4    3 + 2 = 5    3 + 3 = 6    ⋯`{.agda}

The process continues. On the $m^{\textrm{th}}$ day we will know all the
equations where the first number is less than $m$.

As we can see, the reasoning that justifies inductive and recursive definitions
is quite similar. They might be considered two sides of the same coin.

## The story of creation, finitely {#finite-creation}

The above story was told in a stratified way.  First, we create the _infinite
set_ of naturals. We take that set as given when creating instances of addition,
so even on day one we have an _infinite set_ of instances.

Instead, we could choose to create both the naturals and the instances of
addition at the same time. \needspace{2\baselineskip} Then on any day there
would be only a finite set of instances:

> In the beginning, we know nothing.

Now, we apply the rules to all the judgment we know about.
\needspace{3\baselineskip} Only the base case for naturals applies:

> On the first day, we know zero.
>
> - `0 : ℕ`{.agda}

Again, we apply all the rules we know. \needspace{4\baselineskip} This gives us
a new natural, and our first equation about addition.

> On the second day, we know one and all sums that yield zero.
>
> - `0 : ℕ`{.agda}
> - `1 : ℕ    0 + 0 = 0`{.agda}

Then we repeat the process. We get one more equation about addition from the
base case, and also get an equation from the inductive case,
\needspace{5\baselineskip} applied to equation of the previous day:

> On the third day, we know two and all sums that yield one.
>
> - `0 : ℕ`{.agda}
> - `1 : ℕ    0 + 0 = 0`{.agda}
> - `2 : ℕ    0 + 1 = 1   1 + 0 = 1`{.agda}

\needspace{6\baselineskip} You've got the hang of it by now:

> On the fourth day, we know three and all sums that yield two.
>
> - `0 : ℕ`{.agda}
> - `1 : ℕ    0 + 0 = 0`{.agda}
> - `2 : ℕ    0 + 1 = 1   1 + 0 = 1`{.agda}
> - `3 : ℕ    0 + 2 = 2   1 + 1 = 2    2 + 0 = 2`{.agda}

On the $n^{\textrm{th}}$ day there will be $n$ distinct natural numbers, and
$\frac{n \cdot (n - 1)}{2}$ equations about addition. The number $n$ and all
equations for addition of numbers less than $n$ first appear by day $n+1$. This
gives an entirely finitist view of _infinite sets_ of data and equations
relating the data.

## Writing definitions interactively

\textsf{Agda} is designed to be used with the \textsf{Emacs} text editor, and
the two in combination provide features that help to create definitions and
proofs interactively.

\needspace{3\baselineskip}
Begin by typing:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
m + n = ?
~~~

The question mark indicates that you would like \textsf{Agda} to help with
filling in that part of the code. If you type `C-c C-l` (pressing the control
key while hitting the `c` key followed by the `l` key)
\needspace{3\baselineskip} the question mark will bereplaced:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
m + n = { }0
~~~

The empty braces are called a _hole_, and 0 is a number used for referring to
the hole. The hole will display highlighted in green. \needspace{2\baselineskip}
\textsf{Emacs} will also create a window displaying the text

~~~{.agda}
    ?0 : ℕ
~~~

to indicate that hole 0 is to be filled in with a term of type `ℕ`{.agda}.
Typing `C-c C-f` will move you into the next hole.

We wish to define addition by recursion on the first argument. Move the cursor
into the hole and type `C-c C-c`. \needspace{2\baselineskip} You will be given
the prompt:

~~~
    pattern variables to case (empty for split on result):
~~~

Typing `m` will cause a split on that variable, \needspace{4\baselineskip}
resulting in an update to the code:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
zero  + n = { }0
suc m + n = { }1
~~~

\needspace{4\baselineskip}
There are now two holes, and the window at the bottom tells you the required
type of each:

~~~{.agda}
    ?0 : ℕ
    ?1 : ℕ
~~~

\needspace{5\baselineskip}
Going into hole 0 and type `C-c C-,` will display information on the required
type of the hole, and what free variables are available:

~~~{.agda}
Goal: ℕ
——————————————————————————————
n : ℕ
~~~

This strongly suggests filling the hole with `n`. After the hole is filled, you
can type `C-c C-space`, \needspace{4\baselineskip} which will remove the hole:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
zero + n = n
suc m + n = { }1
~~~

Again, going into hole 1 and type `C-c C-,` will display information on the
required type of the hole, \needspace{5\baselineskip} and what free variables
are available:

~~~{.agda}
Goal: ℕ
——————————————————————————————
n : ℕ
m : ℕ
~~~

Going into the hole and type `C-c C-r` will fill it in with a constructor (if
there is a unique choice) or tell you what constructors you might use, if there
is a choice. \needspace{2\baselineskip} In this case, it displays the following:

~~~
Don't know which constructor to introduce of zero or suc
~~~

\needspace{4\baselineskip}
Filling the hole with `suc ?` and typing `C-c C-space` results in the following:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc { }1
~~~

\needspace{5\baselineskip}
Going into the new hole and typing `C-c C-,` gives similar information to
before:

~~~{.agda}
Goal: ℕ
——————————————————————————————
n : ℕ
m : ℕ
~~~

\needspace{5\baselineskip}
We can fill the hole with `m + n` and type `C-c C-space` to complete the
program:

~~~{.agda}
_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc (m + n)
~~~

Exploiting interaction to this degree is probably not helpful for a program this
simple, but the same techniques can help with more complex programs. Even for a
program this simple, using `C-c C-c` to split cases can be helpful.

## More pragmas

Including the lines

```agda
{-# BUILTIN NATPLUS  _+_ #-}
{-# BUILTIN NATTIMES _*_ #-}
{-# BUILTIN NATMINUS _∸_ #-}
```

tells Agda that these three operators correspond to the usual ones, and enables
it to perform these computations using the corresponding \textsf{Haskell}
operators on the arbitrary-precision integer type. Representing naturals with
`zero` and `suc` requires time proportional to $m$ to add $m$ and $n$, whereas
representing naturals as integers in \textsf{Haskell} requires time proportional
to the larger of the logarithms of $m$ and $n$. Similarly, representing naturals
with `zero` and `suc` requires time proportional to the product of $m$ and $n$
to multiply $m$ and $n$, whereas representing naturals as integers in
\textsf{Haskell} requires time proportional to the sum of the logarithms of $m$
and $n$.

#### Exercise `Bin` \textsf{(stretch)} {#Bin}

A more efficient representation of natural numbers uses a binary rather than a
unary system. \needspace{5\baselineskip} We represent a number as a bitstring:

```agda
data Bin : Set where
  nil : Bin
  x0_ : Bin → Bin
  x1_ : Bin → Bin
```

For instance, the bitstring `1011` standing for the number eleven is encoded,
right to left, as `x1 x1 x0 x1 nil`. Representations are not unique due to
leading zeros. Hence, eleven is also represented by `001011`, encoded as `x1 x1
x0 x1 x0 x0 nil`.

Define a function

```agda
inc : Bin → Bin
inc n = ?
```

that converts a bitstring to the bitstring for the next higher number. For
example, since `1100` encodes twelve, we should have:

```agda
_ : inc (x1 x1 x0 x1 nil) ≡ x0 x0 x1 x1 nil
_ = ?
```

Confirm that this gives the correct answer for the bitstrings encoding zero
through four.

Using the above, define a pair of functions to convert between the two
representations.

```agda
to   : ℕ → Bin
to n = ?
from : Bin → ℕ
from n = ?
```

For the former, choose the bitstring to have no leading zeros if it represents a
positive natural, and represent zero by `x0 nil`. Confirm that these both give
the correct answer for zero through four.

```agda
_ : to zero ≡ x0 nil
_ = ?
_ : to (suc zero) ≡ x1 nil
_ = ?
```

## Unicode

\needspace{4\baselineskip}
This chapter uses the following unicode:

~~~
ℕ  U+2115  DOUBLE-STRUCK CAPITAL N           (\bN)
→  U+2192  RIGHTWARDS ARROW                  (\to, \r, \->)
∸  U+2238  DOT MINUS                         (\.-)
≡  U+2261  IDENTICAL TO                      (\==)
⟨  U+27E8  MATHEMATICAL LEFT ANGLE BRACKET   (\<)
⟩  U+27E9  MATHEMATICAL RIGHT ANGLE BRACKET  (\>)
∎  U+220E  END OF PROOF                      (\qed)
~~~

Each line consists of the Unicode character (`ℕ`), the corresponding code point
(`U+2115`), the name of the character (`DOUBLE-STRUCK CAPITAL N`), and the
sequence to type into \textsf{Emacs} to generate the character (`\bN`).

The command `\r` gives access to a wide variety of rightward arrows. After
typing `\r`, one can access the many available arrows by using the left, right,
up, and down keys to navigate. The command remembers where you navigated to the
last time, and starts with the same character next time. The command `\l` works
similarly for left arrows.

\needspace{5\baselineskip}
In place of left, right, up, and down keys, one may also use control characters:

~~~
C-b  left (backward one character)
C-f  right (forward one character)
C-p  up (to the previous line)
C-n  down (to the next line)
~~~

We write `C-b` to stand for control-b, and similarly. One can also navigate left
and right by typing the digits that appear in the displayed list.
